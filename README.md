# Graphs in the wild: an nPlan exploration.

## Problems
1. PPI graph data with text node features. How can we integrate (in interesting ways) this new text information in the graph structure?

2. Graph neural network design that is best suited for this data.


## Solutions approach

#### Assumptions

After reading the paper and inspecting the dataset provided I have made the assumption that we are still interested in some aspects of the original dataset, as the original node features which represent the human genes and also the edge connections. I have noticed that the hierarchy of the tissue graphs is not present so I will assume that these graphs are entirely disconnected.

#### Possbile explorations

1. Train a topic model on all the training text data and extract the top (or top-n) topic for each node text.
  - One-hot encode these node topics and use them instead of the provided labels data. This will allow us to find topics of new graphs that do not have text information, but only the interaction and node features.
  - Use the topics as edge attributes either by taking advantage of the fact the the graph is directed and encode source -> target with the source topic or combine the topics from both and encode the endges between all nodes with the concatenated vectors from both. This will allow us to infer edge information of new graphs. Since we have a one-hot encoding for the topics we can add the topics and treat it as a multiclass problem, this will tell us that the interaction between nodes can belong to two topics.
  - Use the node topics as secondary node features (by building a heterogeneous graph), this will allow us to do multiple node inference and get either the node features, topics or both.
  
2. Instead of topic moddeling we could try to use sentence embeddings and train on those vectors.

3. We could experiment with a graph recurrent network (or other types of graph neural networks) as these graphs have a degree of connection.


## Get the code running

To start the project and install all the dependencies please go into the ``./container`` folder and run ``make lab``.

This will start a docker instance, install all the required packages and provide you with an interactive shell once it is done.

You can start a jupyter notebook instance inside the shell by typing ``jupyter-notebook``. This will be available at http://localhost:8888, with the password: **q1w2e3**. There are mode informations about how this containerised environment works in ``./container/README.md``.

All code can be found in one jupyter notebook ``explorations.ipynb`` and in ``utils.py``. The code to test the model is in ``test.ipynb``

Loading the models and testing the results can be found in ``test.ipynb``.

It was fun to work on this, thanks guys!