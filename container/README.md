## Utility to setup containerized projects

Simply clone the repository in your project's folder

edit Dockerfile to have your required image and OS settings

edit Makefile to use your specific GPUs, or pass them as arguments to make. These are GPU=

start the container

```bash
make lab GPU=0,2
```

You will have a new shell inside the container and all your files will be in the /lab folder.

Enjoy!