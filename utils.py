import json
import torch
import numpy as np

import torch.nn.functional as F

from torch_geometric.nn import GATConv
from torch_geometric.data import Data, Batch

# extract the data and separate it into it's individual graphs
def extract_data(kind='train'):
    feats = np.load(f'ppi/{kind}_feats.npy')
    targs = np.load(f'ppi/{kind}_labels.npy')
    graph_id = np.load(f'ppi/{kind}_graph_id.npy')
    graph = json.load(open(f'ppi/{kind}_graph.json'))

    adj = []
    fts = []
    trg = []
    txt = []

    last_node = 0

    for i in np.unique(graph_id):
        edges = []
        
        ids = np.where(graph_id == i)[0]
        adj += [[]]
        txt += [ [n['text'] for i, n in enumerate(graph['nodes']) if i in ids] ]

        for lnk in graph['links']:
            if lnk['source'] in ids:
                edges.append(lnk['target'] in ids)
                adj[-1].append([lnk['source']-last_node, lnk['target']-last_node])

        last_node += np.max(adj[-1])+1
        print(i, 'N', len(ids), 'L', len(edges), np.all(edges), np.min(adj[-1]), np.max(adj[-1]), last_node)

        fts += [ torch.tensor(feats[ids], dtype=torch.float32) ]
        trg += [ torch.tensor(targs[ids], dtype=torch.float32) ]
        adj[-1] = torch.tensor(adj[-1], dtype=torch.long).T.contiguous()

    return fts, txt, trg, adj


# function that transforms topics into one-hot vectors using the trained model
def make_topic_targets(data, model):
    
    return [ F.one_hot(torch.tensor([model.query_topics(gg, 1)[-1][0] for gg in g]),
                       num_classes=model.get_num_topics()).to(torch.float32) for g in data ]


def build_geometric_data(fts, trg, adj):
    data = [Data(x=x, y=y, edge_index=adj) for x, y, adj in zip(fts, trg, adj)]
    
    return Batch().from_data_list(data)


# create a simple graph attention network
class GAT(torch.nn.Module):
    def __init__(self, node_features, out_features):
        super(GAT, self).__init__()
        self.hid = 8
        self.in_head = 8
        self.out_head = 1
        
        self.conv1 = GATConv(node_features, self.hid, heads=self.in_head, dropout=0.6)
        self.conv2 = GATConv(self.hid*self.in_head, out_features, concat=False,
                             heads=self.out_head, dropout=0.6)

    def forward(self, data):
        x, edge_index = data.x, data.edge_index
                
        x = F.dropout(x, p=0.6, training=self.training)
        x = self.conv1(x, edge_index)
        x = F.elu(x)
        x = F.dropout(x, p=0.6, training=self.training)
        x = self.conv2(x, edge_index)
        
        return F.log_softmax(x, dim=1)
    
